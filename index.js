const express = require('express'); //import the Express package from node_modules
const app = express(); //store Express' express function in a variable 
const mongoose = require('mongoose') //import Mongoose into our project
const cors = require('cors') //import cors into our project

/*const corsOptions = {
	origin: ["http://localhost:3000", "https://capstone3-client-iw0pua69z.vercel.app/"],
	optionsSuccessStatus: 200,
}*/

//corsOptions

app.use(express.json()) //Middleware - Only allows requests if they are in JSON format
app.use(express.urlencoded({ extended: true })) //allow POST requests to include nested objects
app.use(cors())

//mongo db url //''
const connection = "mongodb+srv://giemel:12345@cluster0.sm9ei.mongodb.net/capstone3"
					

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect(`${connection}?retryWrites=true&w=majority`, {
				
	useNewUrlParser: true,
	useUnifiedTopology: true
})

const userRoutes = require('./routes/user')//import our routes so that we may be able to use the routes to where we can make our requests to this server to make its responses.

app.use('/api/users', userRoutes)//app.use() is our middleware, '/users' is our path and our provided router from our userRoutes, which means it is where we put our endpoints through
//http://localhost:4000/api/users/<endpoint>/<route>

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
}) //for starting the server

