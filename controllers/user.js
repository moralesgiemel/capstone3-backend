/*Controllers - are mostly functions which are then exported to be used in another file and that is in our routes*/

const User = require("../models/User")
//require:bycrypt
const bcrypt = require("bcrypt")

//if the module is from npm we only need to put the name of the package, on teh other hand if the file/module is custom made, we have to put the path of the file.
//Functions
//Check if an email already exists

const auth = require("../auth")

//This will allow us to use Google Login Client and verify the token.
const {OAuth2Client} = require('google-auth-library') 
const clientId = '146578207552-qmaav34o6s3k586s2nl1i9sfl080reg0.apps.googleusercontent.com'
//Reminder: The Client ID here in the backend MUST match the client ID used in te frontned.
//It may not cause immediate error during local development but it flares up an error when hosted.
/*const { google } = require("googleapis")
const OAuth2 = google.auth.OAuth2*/
/*require('dotenv').config()*/

//Functions
//Check if an email alredy exists
module.exports.emailExists = (parameterFromRoute) => {

	//Steps
	/*
		1. Use mongoose's find() with the search criteria for email.
		2. then() - runs a codeblock once a method wherein it was attached was finished its process and returned a response or a result
		3. using the result from find, we're going to check if the length of the array that is going to be returend by the query has a length of 0 or not.
		4. user a ternary operator which will return either true or false.

	*/

	return User.find({ email: parameterFromRoute.email}).then(resultFromFind => {

		return resultFromFind.length > 0 ? true : false
	})
}


module.exports.register = (params) => {

	let newUser = new User({

		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		password: bcrypt.hashSync(params.password, 10),
		//hashSync() hashes/encrypt our string provided to the number of salt, in our case: 10.
		loginType: "regular"
	})

	return newUser.save().then((user, err)=>{

		return(err) ? false : true

	})
}

module.exports.createCategory = (params) => {

	return User.findById(params._id).then(resultFromFindById=> {

		resultFromFindById.categories.push({category: params.category, categoryType: params.categoryType})

		return resultFromFindById.save().then((resultFromSaveChanges, err)=> { return (err) ? false : true })
		
	})

}


module.exports.createRecord = (params) => {

	return User.findById(params._id).then(resultFromFindById=> {

		resultFromFindById.records.push({

			categoryType: params.categoryType,
			
			categoryName: params.categoryName, 
			
			amount: params.amount, 
				
			description: params.description,
			
			balance: params.balance
				
		})

		return resultFromFindById.save().then((resultFromSaveChanges, err)=> { return (err) ? false : true })
		
	})

}



module.exports.getAllUsers = () => {

	return User.find({}).then(resultFromFind => resultFromFind)

}

/*
module.exports.getAllExpenses = () => {
 
		return User.find({ "categoryType": "Expense" }).then(resultFromFind => resultFromFind)
}*/


//login
module.exports.login = (params) => {
			//mongoose query - findOne() - it is much efficient to current use to find a single document.
			//If you're in doubt about the value of a variable, console.log()
	return User.findOne({email: params.email}).then(resultFromFindOne => {

		/*console.log(resultFromFindOne)*/

		//there is no user of the same email, run this code:
		if(resultFromFindOne == null){
			return false
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password)
		//compareSync() is used to compare our users' password. The one that they input against the hashed password saved in our database.
		console.log(isPasswordMatched)

		if(isPasswordMatched){

			return { accessToken: auth.createAccessToken(resultFromFindOne)}
			//Create access token is a function which will return a JWT or a an access token using the data from the result of our findOne.
		} else {

			return false
		}
	})
}


module.exports.getUser = (params) => {


	console.log(params) //{userId: "12312331"}


	return User.findById(params.userId).then(resultFromFindById=> {

		//console.log(resultFromGetDetails)
		/*const details  = {

			_id: resultFromGetDetails.id,
			isAdmin: resultFromGetDetails.isAdmin,
			firstName: resultFromGetDetails.firstName,
			lastName: resultFromGetDetails.lastName,
			email: resultFromGetDetails.email,
			mobileNo: resultFromGetDetails.mobileNo,
			enrollments: resultFromGetDetails.enrollments


		}*/

		//return details
		
		resultFromFindById.password = undefined

		console.log(resultFromFindById.records)

		return resultFromFindById
		
	})
	
		
}

module.exports.getAll = (params) => {


	console.log(params) //{userId: "12312331"}

	return User.findById(params.userId).then(resultFromFindById=> {

		return resultFromFindById.records
		
	})
			
}

module.exports.getAllIncome = (params) => {


	console.log(params)

	return User.findById(params.userId).then(resultFromFindById=> {			
			
			let tempIncome = []
			resultFromFindById.records.forEach(element =>{
				
				if(element.categoryType==="Income"){
					tempIncome.push(element)
				}
			})

			return tempIncome					
	})
		
}


module.exports.getAllExpenses = (params) => {


	console.log(params)

	return User.findById(params.userId).then(resultFromFindById=> {			
			
			let tempExpense = []
			resultFromFindById.records.forEach(element =>{
				
				if(element.categoryType==="Expense"){
					tempExpense.push(element)
				}
			})

			return tempExpense
					
	})
	
		
}


module.exports.userUpdate = (userId, userDetails)=> {

	let newDetails = {

		firstName: userDetails.firstName,
		lastName: userDetails.lastName,
		mobileNo: userDetails.mobileNo
	}

	return User.findByIdAndUpdate(userId, newDetails).then((user, err)=>{

		return (err) ? false : true

	})
}


//Google Login
module.exports.verifyGoogleTokenId = async ({tokenId}) => {

	//console.log(tokenId)
	//console.log(clientId)

	//console.log(tokenId)
	//create a new OAuth2Client with our clientId for identification
	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({idToken: tokenId, audience: clientId})
	//audience and idToken are required to check your users' google login token and your OAuthClient's 
	//clientdId side by side to check if it's coming from a recognizable source.

	//let's check if the data responded by verifyIdToken is correct.

	


	if(data.payload.email_verified === true){

		//check the db if the email found in the data.payload sent by google is already registerd in our db:
		const user = await User.findOne({email: data.payload.email}).exec()
		//.exec() in mongoose, works almost like then() that it allows the execution of the following statements.
		//if the user variable logs null, then the use is not registered in OUR db yet.
		//if the user variable logs details, then the user is already registered in our DB
		//console.log(user)
		//Check if user is null,or not:
		if(user !== null){
			
			console.log('A user with the same email has been registered.')
			if(user.loginType === "google"){

				return {accessToken: auth.createAccessToken(user)}

			} else {

				return {error: 'login-type-error'}
			}
		
		} else {
			//register the first-time our google login user, logs into our app.

			//check in the console the incoming payload of the user's data from google.
			//console.log(data.payload)

			const newUser = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: "google"
			})

			//saves our new user into our db
			return newUser.save().then((user, err) => {

				//create a token from our auth module
				return {accessToken: auth.createAccessToken(user)}
			})
		}
	
	} else { //google auth error if the google tokenId is compromised or there had been an error while the tokenId.
		
		return {error: "google-auth-error"}
	}


}